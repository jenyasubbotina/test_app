import 'dart:convert';
import 'package:http/http.dart' as http;

class ApiService {

  final String apiKey = 'QFWW2C2WKVY8DM9VCFV7FY94S';
  final String contentType = 'json';
  final String baseUrl = 'https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/timeline/';

  Future<double> getWeatherDataByCity({required String city}) async {
    String url = baseUrl + city + '?unitGroup=us&key=$apiKey&contentType=$contentType';
    final response = await http.get(
        Uri.parse(url)
    );
    if (response.statusCode == 200) {
      dynamic json = jsonDecode(response.body);
      if (json != null) {
        List<dynamic> days = json['days'];
        Map<String, dynamic> firstDay = days.first;
        double temp = firstDay['temp'];
        return temp;
      }
    }
    return 0;
  }

}