import 'package:application_challenge/app/app.locator.dart';
import 'package:application_challenge/core/api_service.dart';
import 'package:application_challenge/ui/views/home/home_view.form.dart';
import 'package:stacked/stacked.dart';

class HomeViewModel extends FormViewModel {
  final _weatherApi = locator<ApiService>();

  double? _temperature;

  double? get temperature => _temperature;

  Future<void> getWeatherData() async {
    if (cityValue != null && cityValue!.isNotEmpty) {
      final result =
      await _weatherApi.getWeatherDataByCity(city: cityValue.toString());
      convertResult(result);
      notifyListeners();
    }
  }

  void convertResult(double temp) {
    _temperature = ((temp - 32) / 1.8).roundToDouble();
    notifyListeners();
  }

  @override
  void setFormStatus() {}
}
